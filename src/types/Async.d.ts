export declare global {
  type Async<TFn> = TFn extends (...a: infer A) => any ? (...a: A) => Promise<ReturnType<TFn>> : never;
}
