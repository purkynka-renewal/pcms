import { EventSubscriber, On } from "event-dispatch";
import Debug from "debug";
import events from "./events";

const debug = Debug("App");
const debugHttp = debug.extend("http");

@EventSubscriber()
export default class ServiceEvents {
  @On(events.http.error)
  onHTTPError(err: Error) {
    debugHttp(err);
  }
}
