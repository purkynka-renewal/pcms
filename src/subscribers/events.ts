export default {
  http: {
    error: "onHTTPError",
  },
  files: {
    updated: "onFilesUpdated",
  },
  content: {
    updated: "onContentUpdated",
  },
};
