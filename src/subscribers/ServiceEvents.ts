import { EventSubscriber, On } from "event-dispatch";
import { Container } from "typedi";
import type { Socket } from "socket.io";
import events from "./events";

@EventSubscriber()
export default class ServiceEvents {
  @On(events.files.updated)
  onFilesUpdated(path: string) {
    Container.get<Socket>("socket").emit("files:updated", path);
  }

  @On(events.content.updated)
  onContentUpdated({ modelName, id }: { modelName: string, id: number }) {
    Container.get<Socket>("socket").emit("content:updated", modelName, id);
  }
}
