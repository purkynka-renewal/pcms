import { Service } from "typedi";
import { EventDispatcher } from "event-dispatch";
import { PrismaClient } from "@prisma/client";
import APIError from "App/api/APIError";
import events from "App/subscribers/events";

import SiteModel from "App/models/site";

export const models = {
  site: SiteModel,
};
export type ModelName = keyof typeof models;

@Service()
export default class ContentService {
  private readonly emitter = new EventDispatcher();

  constructor(
    private readonly prisma: PrismaClient,
  ) { }

  async get(modelName: ModelName, id?: any, fields?: string[]) {
    const model = this.getModel(modelName);

    // Singleton
    if (model.singleton) {
      return this.prisma[modelName].findFirst({
        select: fieldsToSelect(fields),
      });
    }

    // Multiton
    return this.prisma[modelName].findUnique({
      where: { [model.idField]: id },
      select: fieldsToSelect(fields),
    });
  }


  async set(modelName: ModelName, patch: Record<string, any>, id?: any, fields?: string[]) {
    const model = this.getModel(modelName);

    // Check the patch
    for (const field of Object.keys(patch)) {
      let editable = false;
      // Editable fields are ok
      if (model.editableFields.includes(field as any)) editable = true;
      // Connectable fields need pre-processing
      // else if (model.components.hasOwnProperty(field)) {
      //   editable = true;
      //   patch[field] = { connect: { [model.connectableFields[field]]: patch[field] } };
      // }
      // Other fields cancel the whole transaction
      if (!editable) throw new APIError("fieldNotEditable", 400);
    }

    // If singleton, get the row's ID
    if (model.singleton) {
      const singleton = await this.prisma[modelName].findFirst({
        select: { [model.idField]: true },
      }) as Record<string, any>;
      if (!singleton) return null;

      id = singleton[model.idField] as string | number;
    }

    const promise = this.prisma[modelName].update({
      where: { [model.idField]: id },
      data: patch,
      select: fieldsToSelect(fields) || { [model.idField]: true },
    });

    promise.then(() => {
      this.emitter.dispatch(events.content.updated, { modelName, id });
    });

    return promise;
  }

  getModel(modelName: ModelName) {
    if (!models.hasOwnProperty(modelName)) throw new APIError("unknownModel", 400);
    const model = models[modelName];
    if (!this.prisma.hasOwnProperty(modelName)) throw new APIError("tableDoesntExist", 500);
    return model;
  }
}


export function fieldsToSelect(fields?: string[]) {
  return !fields ? undefined : fields.reduce<Record<string, true>>((acc, field) => {
    acc[field] = true;
    return acc;
  }, {});
}
