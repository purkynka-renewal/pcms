import Path from "path";
import fs from "fs";
import { EventDispatcher } from "event-dispatch";
import { Service } from "typedi";
import { PrismaClient, File, Media } from "@prisma/client";
import Mime from "mime/lite";
import APIError from "App/api/APIError";
import events from "App/subscribers/events";
import Config from "App/config";
import { probeMime, probeMedia, scaleImage, generateThumbnail } from "./subprocesses";

@Service()
export default class FileService {
  private readonly emitter = new EventDispatcher();

  constructor(
    private readonly prisma: PrismaClient,
    private readonly config: Config,
  ) { }

  async getId(id: number | string) {
    id = isNaN(+id) ? id + "" : +id;
    if (typeof id === "string") return this.traverseFolders(id);
    return id;
  }

  private async getFilePath(id: string) {
    return this.getUploadFilepath(await this.prisma.file.findUnique({
      where: { id },
      select: { id: true, ext: true },
      rejectOnNotFound: true,
    }));
  }

  async get(id: string) {
    return this.prisma.file.findUnique({
      where: { id },
      include: { media: true },
    });
  }

  getImageSizes() {
    return this.config.images.sizes;
  }

  async list(id: number | string) {
    id = await this.getId(id);

    return this.prisma.folder.findUnique({
      where: { id },
      select: {
        files: {
          include: {
            media: true,
          }
        },
        folders: true,
      },
    });
  }


  async rename(id: string | number, name: string) {
    if (!name) return;
    const query = {
      data: { name },
      select: { id: true },
    };

    if (!isNaN(+id)) id = +id;

    if (typeof id === "number") await this.prisma.folder.update({ where: { id }, ...query });
    else if (typeof id === "string") await this.prisma.file.update({ where: { id }, ...query });
    this.emitter.dispatch(events.files.updated, id);
  }


  async mkdir(path: string) {
    const result = await this.traverseFolders(path.replace(/\/+/g, "/"), true);
    this.emitter.dispatch(events.files.updated, result);
    return result;
  }



  async rm(id: string | number) {
    if (typeof id === "number" || !isNaN(+id)) {
      return this.rmdir(+id);
    }
    const result = await this._rm(id);
    this.emitter.dispatch(events.files.updated, { id });
    return result;
  }

  private async _rm(id: string) {
    const file = await this.prisma.file.findUnique({
      where: { id },
      include: {
        media: true,
      },
      rejectOnNotFound: true,
    });

    if (file.media) await this.prisma.media.delete({ where: { id }, select: { id: true } });
    await this.prisma.file.delete({ where: { id }, select: { id: true } });

    await this.unlink(file);
    return true;
  }

  private async unlink(file: File & { media: Media | null }) {
    return Promise.all([
      fs.promises.unlink(this.getUploadFilepath(file)),
      file.media && file.media.thumbnail ?
        fs.promises.unlink(Path.join(this.getUploadFilepath(), file.media.thumbnail))
        : undefined,
    ]);
  }

  // WARNING: Will burn your house down on a complex folder structure
  async rmdir(aId: number | string) {
    const id = await this.getId(aId);

    const traverse = async (_id: number) => {
      const folder = (await this.prisma.folder.findUnique({
        where: { id: _id },
        select: {
          files: { select: { id: true } },
          folders: { select: { id: true } },
        },
      }))!;

      // Unlink all files in folder
      const deletePromises: Promise<unknown>[] = folder.files.map(file => this._rm(file.id));

      // Continue with deeper folders
      for (const subfolder of folder.folders) {
        deletePromises.push(traverse(subfolder.id));
      }
      await Promise.all(deletePromises);

      // Finally delete the folder itself
      return this.prisma.folder.delete({
        where: { id },
        select: { id: true },
      });
    };

    await traverse(id);
    this.emitter.dispatch(events.files.updated, { id });
    return true;
  }


  async traverseFolders(path: string, mkdir = false) {
    let lastResult = { id: 1 };

    // Recursively create folders
    for (const folder of Path.normalize(path).split("/").filter(v => v && !v.match(/^(\.+)$/))) {
      const where = {
        name_parentId: {
          name: folder,
          parentId: lastResult.id,
        },
      };

      if (mkdir) {
        lastResult = await this.prisma.folder.upsert({
          where,
          create: {
            name: folder,
            parent: { connect: lastResult },
          },
          update: {},
          select: { id: true },
        });
      }
      else {
        lastResult = await this.prisma.folder.findUnique({
          where,
          select: { id: true },
          rejectOnNotFound: true,
        });
      }
    }

    return lastResult.id;
  }


  async upload(currentPath: string, targetPath: string) {
    try {
      // Prevent exiting the root
      if (Path.normalize(targetPath).startsWith("..")) throw new APIError("Invalid Path", 400);

      // Parse target path
      const filename = Path.basename(targetPath).trim();
      const virtDirname = Path.normalize(Path.dirname(targetPath).replace(/^\/|\/$/, ""));
      if (!filename) throw new APIError("Filename Expected", 400);

      // Register in DB
      const file = await this.create(currentPath, virtDirname, filename);
      const publicPath = this.getUploadFilepath(file);

      // Make sure target folder exists
      if (!fs.existsSync(this.getUploadFilepath())) fs.mkdirSync(this.getUploadFilepath(), { recursive: true });
      // Move the new file to it's location
      await fs.promises.copyFile(currentPath, publicPath);

    } finally {
      // Make sure the original file doesn't exist
      await fs.promises.unlink(currentPath);
    }
  }


  async create(realPath: string, virtDirname: string, filename: string) {
    // Create needed folders
    const mkdirPromise = this.traverseFolders(virtDirname, true);
    // Get file info
    const statPromise = fs.promises.stat(realPath);
    const mime = await probeMime(realPath);
    const isVideo = mime.startsWith("video");
    const isMedia = isVideo || mime.startsWith("image");

    // Await multiple stuff
    const [media, { size }, folderId] = await Promise.all([
      // Get media info if needed
      !isMedia ? null : probeMedia(realPath, isVideo),
      // Await file stat
      statPromise,
      // Await folders
      mkdirPromise,
    ]);

    const result = await this.prisma.file.create({
      data: {
        name: filename,
        ext: Mime.getExtension(mime) ?? Path.extname(filename),
        size,
        mime,
        folder: { connect: { id: folderId } },
        media: media ? {
          create: {
            ...media,
            type: isVideo ? "VIDEO" : "IMAGE",
          }
        } : undefined,
      },
      select: { id: true, ext: true },
    });

    // thumbnails for videos
    if (isVideo) await this.generateThumbnail(result.id, realPath);

    this.emitter.dispatch(events.files.updated, result);
    return result;
  }


  async optimizeImage(id: string, size: number, sizing?: "increase" | "decrease" | null, filepath?: string) {
    if (!this.config.images.sizes.includes(+size)) throw new APIError("Invalid size", 404);

    const file = await this.prisma.file.findUnique({
      where: { id },
      rejectOnNotFound: true,
      include: { media: true },
    });
    if (!file.media) throw new APIError("File is not media", 404);
    if (!filepath) filepath = this.getUploadFilepath(file);
    if (!sizing) sizing = this.config.images.sizing;

    if (file.media.type === "VIDEO") {
      if (!file.media.thumbnail) {
        const thumbnail = await this.generateThumbnail(id, filepath);
        if (!thumbnail) throw new APIError("Failed to generate thumbnail", 500);
        filepath = thumbnail;
      }
      else filepath = file.media.thumbnail;
      filepath = Path.join(this.getUploadFilepath(), filepath);
    }

    await fs.promises.mkdir(this.getImageCacheFilepath(size), { recursive: true });

    const output = this.getImageCacheFilepath(size, file);
    return await scaleImage(filepath, output, size, sizing) && output;
  }


  async generateThumbnail(id: string, filepath?: string | null) {
    if (!filepath) filepath = await this.getFilePath(id);

    // Check if file exists
    if (!fs.existsSync(filepath)) throw new APIError("File doesn't exist", 404);

    // Create thumbnail filename
    const thumbnail = id + ".thumbnail.webp";
    const thumbnailPath = Path.join(this.getUploadFilepath(), thumbnail);

    // Generate thumbnail
    const size = this.config.images.videoThumbSize;
    const sizing = this.config.images.sizing;
    const result = await generateThumbnail(filepath, thumbnailPath, size, sizing);

    // Update DB
    if (result) await this.prisma.media.update({
      where: { id },
      data: { thumbnail },
    });
    return result && thumbnail;
  }

  getUploadFilepath(file?: { id: string, ext: string }) {
    return Path.join(
      process.cwd(),
      this.config.uploadPath,
      file ? file.id + "." + file.ext : "",
    );
  }
  getImageCacheFilepath(size?: number, file?: { id: string }) {
    return Path.join(
      process.cwd(),
      this.config.publicCachePath,
      size + "",
      size && file ? file.id + ".webp" : "",
    );
  }
}
