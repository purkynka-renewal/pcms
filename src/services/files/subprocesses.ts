import { spawn } from "child_process";
import Debug from "debug";

const debug = Debug("Files");
const mimeDebug = debug.extend("mimeProbe");
const mediaDebug = debug.extend("mediaProbe");
const imageDebug = debug.extend("images");
const thumbDebug = debug.extend("thumbnails");

/**
 * probeMime
 */
export function probeMime(path: string): Promise<string> {
  let output = "";
  const file = spawn("file", [
    "-bnN",
    "--mime-type",
    path
  ]);

  file.stderr.on("data", mimeDebug);
  file.stdout.on("data", data => output += data.toString("utf-8"));
  return new Promise((resolve, reject) => file.on("close", code => {
    output = output.trim();
    if (code !== 0 || !output) return reject("Mime probing failed");

    resolve(output);
  }));
}

/**
 * probeMedia
 */
export function probeMedia(path: string, video: boolean): Promise<{ width: number, height: number, length?: number }> {
  let output = "";
  const ffprobe = spawn("ffprobe", [
    "-v", "fatal",
    "-select_streams", "v:0",
    "-print_format", "json",
    "-show_format",
    "-show_entries", "stream=width,height",
    path
  ]);

  ffprobe.stderr.on("data", mediaDebug);
  ffprobe.stdout.on("data", data => output += data.toString("utf-8"));
  return new Promise((resolve, reject) => ffprobe.on("close", code => {
    const data = JSON.parse(output.trim());
    if (code !== 0 || !data.hasOwnProperty("streams") || !data.streams.length) return reject("Media probing failed");

    const [stream] = data.streams;
    resolve({
      width: stream.width,
      height: stream.height,
      length: video ? parseFloat(data.format.duration) : undefined,
    });
  }));
}

/**
 * scaleImage
 */
export function scaleImage(path: string, output: string, size: number, sizing: "increase" | "decrease") {
  const magick = spawn("convert", [
    `${path}[0]`,
    "-resize", `${size}x${size}${sizing === "increase" ? "^" : ""}>`,
    output
  ]);
  magick.stderr.on("data", imageDebug);
  magick.stdout.on("data", imageDebug);

  return new Promise<boolean>(resolve => magick.on("close", code => {
    resolve(!code);
  }));
}

/**
 * generateThumbnail
 */
export function generateThumbnail(path: string, output: string, size: number, sizing: "increase" | "decrease") {
  const ffmpeg = spawn("ffmpeg", [
    "-i", path,
    "-map", "v:0",
    "-v", "fatal",
    "-vf", `scale=${size}:${size}:force_original_aspect_ratio=${sizing}`,
    "-pix_fmt", "yuv420p",
    "-frames", "1",
    "-y", output
  ]);
  ffmpeg.stderr.on("data", thumbDebug);
  ffmpeg.stdout.on("data", thumbDebug);

  return new Promise<boolean>(resolve => ffmpeg.on("close", code => {
    resolve(!code);
  }));
}
