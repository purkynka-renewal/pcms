import { randomBytes } from "crypto";
import { Service } from "typedi";
import bcrypt from "bcrypt";
import { PrismaClient } from "@prisma/client";
import Session from "./Session";

export type { Session };

@Service()
export default class AuthService {
  private readonly sessions = new Map<string, Session>();

  constructor(
    private readonly prisma: PrismaClient,
  ) { }

  async auth(username: string, passwd: string) {
    return this.prisma.user.findUnique({
      where: { username },
    }).then(async user => {
      if (!user) return false;
      return bcrypt.compare(passwd, user.password);
    });
  }

  createSession() {
    const sessionID = randomBytes(48).toString("base64");
    const session = new Session(this, sessionID, () => this.destroySession(sessionID));

    this.sessions.set(sessionID, session);
    return session;
  }

  restoreSession(sessionID: string) {
    let session = this.sessions.get(sessionID);

    if (!session) session = this.createSession();
    else session.fresh = false;

    return session;
  }

  private destroySession(sessionID: string) {
    this.sessions.delete(sessionID);
  }
}
