import EventEmitter from "events";
import type AuthService from "./";

const deathDelay = 5 * 60 * 1000; // Keep the session alive for 5 minutes after disconnecting
const loggedDeathDelay = 25 * 60 * 60 * 1000; // 25 hours for logged users

export default class Session extends EventEmitter {
  private deathTimeout?: NodeJS.Timeout | null;
  private _dead = false;
  connected = false;
  authorized = false;
  fresh = true;

  constructor(
    private readonly service: AuthService,
    readonly id: string,
    private readonly selfDestruct: () => void
  ) {
    super();
  }

  get dead() { return this._dead; }

  destroy() {
    if (this.dead) return;
    this._dead = true;
    if (this.deathTimeout) clearTimeout(this.deathTimeout);
    this.emit("death");
    this.selfDestruct();
  }

  scheduleDeath() {
    if (this.dead || this.deathTimeout) return;
    this.connected = false;

    this.deathTimeout = setTimeout(
      () => this.destroy(),
      this.authorized ? loggedDeathDelay : deathDelay,
    ).unref();
  }

  cancelDeath() {
    if (this.dead || !this.deathTimeout) return;
    this.connected = true;

    clearTimeout(this.deathTimeout);
    this.deathTimeout = null;
  }

  async login(username: string, passwd: string) {
    return this.service.auth(username, passwd).then(result => {
      if (result) {
        this.authorized = true;
        this.emit("authorized");
      }

      return result;
    });
  }

  logout() {
    this.destroy();
  }
}
