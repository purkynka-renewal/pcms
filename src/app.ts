import "reflect-metadata";
import Debug from "debug";
import loader from "./loaders";
import "./subscribers";

const debug = Debug("App");
debug.enabled = true;

process.title = "pcms";

async function app() {
  debug("Initializing...");
  const { server, config } = await loader();

  // Process events
  process.on("exit", code => {
    debug(`Exitting with code ${code}`);

    if (server.listening) server.close();
  });

  // Server events
  server.on("listening", () => {
    const addr = server.address()!;
    const url = (typeof addr === "string"
      ? "unix:" + addr
      : `http://${addr.address}:${addr.port}${config.baseUrl}`
    );
    debug("Listening on " + url);
  }).on("error", err => {
    debug(err);
    process.exit(1);
  });

  debug("Ready!");

  // Start the server
  if (config.path) server.listen(config.path);
  else server.listen(config.port!, config.host!);
}

app();
