import { Container } from "typedi";
import Path from "path";
import { Server as SocketServer } from "socket.io";
import SocketApi from "App/api/socket";
import type { Server } from "http";

export default (config: { baseUrl: string, socketUrl: string }, server: Server) => {
  const io = new SocketServer(server, {
    serveClient: false,
    transports: ["websocket"],
    path: Path.join(config.baseUrl, config.socketUrl),
  });

  Container.set("socket", io);

  io.on("connection", SocketApi());

  return io;
};
