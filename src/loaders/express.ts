import Express, { ErrorRequestHandler } from "express";
import { EventDispatcher } from "event-dispatch";
import events from "App/subscribers/events";
import HttpRouter from "App/api/http";

export default (config: { baseUrl: string }) => {
  // Init
  const app = Express();
  const eventDispatch = new EventDispatcher();

  // Config
  app.set("trust proxy", "loopback");
  app.set("x-powered-by", false);

  // Setup
  app.use(config.baseUrl, HttpRouter());

  // Error handler
  app.use(((err, _req, res, next) => {
    if (!err) return next();

    eventDispatch.dispatch(events.http.error, err);

    res.status(err.status ?? 500);
    res.json({
      status: err.status ?? 500,
      error: err.type || err.name || "",
      // errorMessage: err.message,
    });
  }) as ErrorRequestHandler);

  return app;
};
