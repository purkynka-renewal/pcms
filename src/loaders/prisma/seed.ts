import bcrypt from "bcrypt";
import type { PrismaClient } from "@prisma/client";

type ConfigStub = { defaultUser: { login: string, password: string } };

export default async (config: ConfigStub, prisma: PrismaClient) => {
  return Promise.all([
    // Site info
    prisma.site.upsert({
      where: { id: 1 },
      update: {},
      create: {
        id: 1,
        name: "SSPBrno.cz",
        shortname: "SSPBrno",
        description: "Střední škola Purkyňova",
        about: "Střední škola v Brně",
      },
    }),

    // Default user
    prisma.user.aggregate({
      count: { username: true },
    }).then(({ count }) => {
      if (count.username) return;
      return bcrypt.hash(config.defaultUser.password, 10).then(password => {
        return prisma.user.create({
          data: {
            username: config.defaultUser.login,
            password,
          },
        });
      });
    }),

    // Root folder
    prisma.folder.upsert({
      where: { id: 1 },
      update: {},
      create: {
        id: 1,
        name: "/",
      },
      select: { id: true },
    }),
  ]);
};
