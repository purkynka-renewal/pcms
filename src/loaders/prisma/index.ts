import { Container } from "typedi";
import { PrismaClient } from "@prisma/client";
import seed from "./seed";

type ConfigStub = { databaseUrl: string, defaultUser: { login: string, password: string } };

export default async (config: ConfigStub) => {
  const prisma = new PrismaClient({
    datasources: { db: { url: config.databaseUrl } },
    errorFormat: process.env.NODE_ENV === "production" ? "minimal" : undefined,
  });

  Container.set(PrismaClient, prisma);

  await seed(config, prisma);

  return prisma;
};
