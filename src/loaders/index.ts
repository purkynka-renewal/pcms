import configLoader from "./config";
import prismaLoader from "./prisma";
import expressLoader from "./express";
import serverLoader from "./server";
import socketLoader from "./socket";

export default async () => {
  const config = configLoader();
  const prismaPromise = prismaLoader(config);
  const app = expressLoader(config);
  const server = serverLoader(app);
  const socket = socketLoader(config, server);

  return {
    config,
    prisma: await prismaPromise,
    app,
    server,
    socket,
  };
};
