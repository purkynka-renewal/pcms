import { Container } from "typedi";
import * as dotenv from "dotenv";
import dotenvExpand from "dotenv-expand";
import Config from "App/config";

export default () => {
  // NOTE: dotenv is redundant, Prisma does the following too, but oh well
  const result = dotenv.config();
  if (result.error) throw result.error;
  const env = dotenvExpand(result).parsed;

  const config = new Config(env);

  Container.set(Config, config);
  return config;
};
