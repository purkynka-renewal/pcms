import { createServer } from "http";
import type { Application } from "express";

export default (app: Application) => createServer(app);
