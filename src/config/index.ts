export default class Config {
  //
  // HTTP SETTINGS
  //

  // Server settings
  path = this.env.SOCKET || null;
  port = this.path ? null : this.env.PORT ? parseInt(this.env.PORT) : 6532;
  host = this.path ? null : this.env.HOST || "localhost";

  // Routing settings
  baseUrl = this.env.BASE_URL || "/";
  socketUrl = this.baseUrl + (this.env.SOCKET_URL || "/socket");

  //
  // CORE SETTINGS
  //

  // Database settings
  databaseUrl = this.env.DATABASE_URL;

  // File path settings
  publicCachePath = this.env.PUBLIC_CACHE_PATH || "public/cache/";
  uploadPath = this.env.UPLOAD_PATH || "public/files/";

  // Default user settngs
  defaultUser = {
    login: this.env.DEFAULT_LOGIN || "purkynka",
    password: this.env.DEFAULT_PASSWORD || "rielae6ux3rofaTi",
  };

  //
  // SERVICE SETTINGS
  //

  // Image optimization settings
  images = {
    videoThumbSize: this.env.VIDEO_THUMB_SIZE ? parseInt(this.env.VIDEO_THUMB_SIZE) : 720,
    sizes: [32, 128, 256, 512, 1024],
    sizing: "increase" as "increase" | "decrease",
  };

  constructor(private readonly env: Record<string, string> = {}) { }
}
