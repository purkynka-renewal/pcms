export default class APIError extends Error {
  constructor(error: string, readonly status: number, message?: string) {
    super(message);
    this.name = error;
  }
}
