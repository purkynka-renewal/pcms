import { Router } from "express";
import rooter from "./routes/root";
import files from "./routes/files";
import images from "./routes/images";

export default function() {
  const router = Router();

  rooter(router);
  files(router);
  images(router);

  return router;
}
