import type { RequestHandler } from "express";
import { Container } from "typedi";
import AuthService from "App/services/auth";

export const authorization: RequestHandler = (req, res, next) => {
  const authService = Container.get(AuthService);
  const session = authService.restoreSession(req.body.sessionID as string);
  if (!session.authorized) return res.status(403).send("Unauthorized");
  next();
};

export default authorization;
