import { RequestHandler } from "express";
import Path from "path";
import fs from "fs";
import { Container } from "typedi";
import FileService from "App/services/files";
import APIError from "App/api/APIError";

export const get: RequestHandler = async (req, res, next) => {
  const fileService = Container.get(FileService);
  const id = req.params.file.split(".")[0];
  const root = fileService.getImageCacheFilepath(+req.params.size);
  let path = Path.join(root, id + ".webp");

  path = Path.resolve(path);
  if (!path.startsWith(root)) throw new APIError("Invalid path", 400);

  if (!fs.existsSync(path)) {
    try {
      const result = await fileService.optimizeImage(id, +req.params.size);
      if (!result) return res.sendStatus(500);
      path = result;
    } catch (err) {
      return next(err);
    }
  }

  if (!fs.existsSync(path)) throw new APIError("Image not found", 404);
  return res.sendFile(id + ".webp", { root });
};
