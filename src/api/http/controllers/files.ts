import { RequestHandler } from "express";
import Path from "path";
import { Container } from "typedi";
import FileService from "App/services/files";
import APIError from "App/api/APIError";

export const upload: Async<RequestHandler> = async (req, res, next) => {
  if (!Array.isArray(req.files) || !req.files.length) throw new APIError("Payload Expected", 400);

  const fileService = Container.get(FileService);
  return Promise.all(req.files.map(file =>
    fileService.upload(
      file.path,
      Path.join(
        req.params[0],
        unescape(Buffer.from(file.originalname, "base64").toString("utf-8")),
      ),
    ),
  )).then(() => {
    res.json({ status: 200, error: null });
  }).catch(next);
};


export const get: RequestHandler = (req, res) => {
  const file = req.url.split("/")[1];
  if (!file) throw new APIError("No file specified", 400);

  const fileService = Container.get(FileService);
  const root = fileService.getUploadFilepath();

  return res.sendFile(file, { root });
};
