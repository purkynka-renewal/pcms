import { Router } from "express";
import * as ImagesController from "../controllers/images";

export default function(app: Router) {
  const router = Router();
  app.use("/images", router);

  router.get("/:size/:file", ImagesController.get);
}
