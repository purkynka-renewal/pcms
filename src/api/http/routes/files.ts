import { Router } from "express";
import multer, { diskStorage } from "multer";
import * as FilesController from "../controllers/files";
import authorization from "../middlewares/authorization";

export default function(app: Router) {
  const router = Router();
  app.use("/files", router);

  // NOTE: Upload size is supposed to be limited by reverse proxy
  const upload = multer({
    storage: diskStorage({}),
    limits: {
      fileSize: 512 * 1024 * 1024,
    },
  });

  router.post("/*",
    authorization,
    upload.array("files"),
    FilesController.upload,
  );

  router.get("/*", FilesController.get);
}
