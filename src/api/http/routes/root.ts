import type { Router } from "express";

export default function(app: Router) {
  app.get("/", (_, res) => res.send("Hello :)"));
}
