import type { Socket } from "socket.io";
import { Container } from "typedi";
import Controller from "../controllers/generic";
import FileService from "App/services/files";

export default function(socket: Socket) {
  const files = Container.get(FileService);
  socket.on("files:get", Controller(files.get.bind(files)));
  socket.on("files:getImageSizes", Controller(files.getImageSizes.bind(files)));
  socket.on("files:list", Controller(files.list.bind(files)));
  socket.on("files:rename", Controller(files.rename.bind(files)));
  socket.on("files:mkdir", Controller(files.mkdir.bind(files)));
  socket.on("files:rmdir", Controller(files.rmdir.bind(files)));
  socket.on("files:rm", Controller(files.rm.bind(files)));
}
