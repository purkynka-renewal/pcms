import type { Socket } from "socket.io";
import { Container } from "typedi";
import Controller from "../controllers/generic";
import ContentService from "App/services/content";

export default function(socket: Socket) {
  const content = Container.get(ContentService);
  socket.on("content:get", Controller(content.get.bind(content)));
  socket.on("content:set", Controller(content.set.bind(content)));
}
