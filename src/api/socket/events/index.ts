import type { Socket } from "socket.io";
import contentEventHandler from "./content";
import filesEventHandler from "./files";

export type EventHandler = (socket: Socket) => void;

const eventHandlers: EventHandler[] = [
  contentEventHandler,
  filesEventHandler,
];

export default eventHandlers;
