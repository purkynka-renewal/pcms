import type { Socket } from "socket.io";
import { Container } from "typedi";
import AuthService from "App/services/auth";
import * as SysController from "./controllers/system";
import eventHandlers from "./events";

export type Callback = (result: any, error?: Error | string) => void;


export default function() {
  return function socketHandler(socket: Socket) {
    const authService = Container.get(AuthService);
    // Create or restore session
    const session = authService.restoreSession(socket.handshake.query.sessionID as string);
    // Prevent multiple connections at once and connecting to dead sessions
    if (session.dead || session.connected) {
      // An error occured, destroy the session
      if (!session.dead) session.destroy();
      socket.disconnect();
      return null;
    }
    // If the session is new, send client it's ID
    if (session.fresh) {
      socket.emit("sys:session", session.id);
    }
    // Cancel session death
    session.cancelDeath();


    //// SESSION EVENTS

    // If sessions dies, it's socket should too
    const onSessionDeath = () => socket.disconnect(true);
    session.once("death", onSessionDeath);

    //// SOCKET EVENTS

    // Listen for socket disconnect
    socket.on("disconnect", reason => {
      if (reason === "server shutting down") return; // Don't bother
      // Schedule session death
      session.scheduleDeath();
      // Remove listeners
      session.off("death", onSessionDeath);
      session.off("authorized", onSessionAuthorized);
    });


    // System event handlers
    socket.once("sys:logout", SysController.logout.bind(session));
    socket.on("sys:login", SysController.login.bind(session));
    socket.on("sys:isAuthorized", SysController.isAuthorized.bind(session));

    //// AUTHORIZED SOCKET EVENTS
    // Once we're authorized, unlock the rest of the API
    const onSessionAuthorized = () => {
      socket.emit("sys:authorized");

      for (const eventHandler of eventHandlers) {
        eventHandler(socket);
      }
    };
    if (session.authorized) onSessionAuthorized();
    else session.once("authorized", onSessionAuthorized);
  };
}
