import type { Callback } from "App/api/socket";
import Debug from "debug";

const debug = Debug("App").extend("Socket");

export default function(service: (...args: any[]) => any) {
  return async function geneticController(...args: any[]) {
    const respond: Callback = args.pop();

    try {
      const result = await service(...args);
      respond(result);
    } catch (err) {
      respond(null, err.name);
      debug(err);
    }
  };
}
