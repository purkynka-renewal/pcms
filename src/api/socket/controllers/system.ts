import type { Session } from "App/services/auth";
import type { Callback } from "App/api/socket";

export function login(this: Session, username: string, password: string, respond: Callback) {
  if (this.authorized) respond(null, "alreadyAuthorized");
  this.login(username, password).then(respond).catch(err => respond(null, err));
}

export function logout(this: Session, respond: Callback) {
  respond(null);
  this.logout();
}

export function isAuthorized(this: Session, respond: Callback) {
  respond(this.authorized);
}
