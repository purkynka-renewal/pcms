import type Model from "../Model";

export default interface Component extends Model<any> {
  singleton?: never;
}
