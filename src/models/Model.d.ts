import type Component from "./components/Componenet";

type Query = Record<string, any>;

export default interface Model<ScalarEnum> {
  idField: string;
  singleton?: boolean;

  editableFields: ScalarEnum[];
  components: Record<string, Component>;

  customQueries?: {
    get?(id?: any, fields?: string[]): Query;
    set?(field: string, value: any, id?: any, fields?: string[]): Query;
  };
}
