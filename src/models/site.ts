import type { Prisma } from "@prisma/client";
import type Model from "./Model";

const Site: Model<Prisma.SiteScalarFieldEnum> = {
  idField: "id",
  singleton: true,

  editableFields: [
    "name", "shortname", "description", "about", "email",
  ],
  components: {},
};

export default Site;
