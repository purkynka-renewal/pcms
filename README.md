# PCMS

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_pcms&metric=alert_status)](https://sonarcloud.io/dashboard?id=purkynka-renewal_pcms)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_pcms&metric=security_rating)](https://sonarcloud.io/dashboard?id=purkynka-renewal_pcms)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_pcms&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=purkynka-renewal_pcms)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=purkynka-renewal_pcms&metric=coverage)](https://sonarcloud.io/dashboard?id=purkynka-renewal_pcms)

P CMS (Purkyňka CMS) is a NodeJS Content Management System made
specifically for my highschool, SPŠ Purkyňova.

## Requirements
- Linux host (WSL might be enough)
- NodeJS v12+
- MySQL database
- ffmpeg
- imagemagick

## Recommended
- Reverse proxy (e.g. NGINX) to protect the app, don't expose it directly to internet.  
    It should ensure:
    - Rate-limiting
    - Upload size limiting
    - Serving static files quicker than `express.static()`
    - Caching

## Instalation
- Clone the repo
- Create database and user in MySQL
- Rename/copy `.env.sample` to `.env` and modify the vars for your needs
- Run `npm ci` to install dependencies
- Run `npm run build` to transpile typescript to javascript
- Done

Run with `npm start`.

`npm run dev` builds the project before running it.

## Documentation
- API
    - [HTTP (REST) API](docs/api/http.md)  
    - [Socket API](docs/api/socket.md)

## Project structure
```text
./
├── dist/   - Transpiled source code ready to be run
├── docs/   - Documentation of the project
├── public/ - Publicly accessible static files
├── src/    - "Raw" source code of the project (TypeScript)
├── .env               - Your environment configuration
├── .env.sample        - Example of .env
├── config.json        - Environment-independent configuration
├── config.sample.json - Example of config.json
└── schema.prisma      - Scheme of database used by PCMS; configuration for prisma.io
```

## Content
- Site - General info about the website
- Posts - Articles and news
- Pages - Static content with the following subtypes:
    - About page - Page with additional info (contacts, etc.)
