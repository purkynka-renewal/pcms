import "reflect-metadata";
import chai from "chai";
import "chai/register-should";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import chaiAsPromised from "chai-as-promised";
import { Container } from "typedi";
import Config from "App/config";
import FileService from "App/services/files";
import AuthService from "App/services/auth";
import FileServiceMock from "./_mocks/FileService";
import AuthServiceMock from "./_mocks/AuthService";

chai.use(sinonChai);
chai.use(chaiAsPromised);

Container.set(AuthService, AuthServiceMock);
Container.set(FileService, FileServiceMock);

beforeEach(() => {
  const config = new Config();

  Container.set(Config, config);
});

afterEach(() => {
  sinon.restore();
});


const sharedSuites = new Map<string, ((this: Mocha.Suite) => void)[]>();

export function describeStructure(names: string[], cb: (this: Mocha.Suite) => void) {
  if (!names.length) return;

  let lastSuite: ((this: Mocha.Suite) => void)[] | undefined;
  for (const name of names) {
    if (!sharedSuites.has(name)) {
      sharedSuites.set(name, []);
      const subSuites = sharedSuites.get(name)!;

      const executor = lastSuite ? lastSuite.push.bind(lastSuite) : setImmediate;
      executor(() => describe(name, function() {
        subSuites.forEach(fn => fn.apply(this));
      }));
    }
    lastSuite = sharedSuites.get(name)!;
  }

  lastSuite!.push(cb);
}
