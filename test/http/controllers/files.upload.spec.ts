import { mockReq, mockRes } from "sinon-express-mock";
import { upload as FilesUpload } from "App/api/http/controllers/files";
import { describeStructure } from "Tests/setup";

describeStructure(["HTTP", "Controllers"], () => {
  describe("FilesController.upload", () => {
    it("should respond with 400 for no files", async () => {
      const req = mockReq({ files: [] });
      const res = mockRes();

      const upload = FilesUpload(req, res, err => { throw err; });
      return upload.should.be.eventually.rejected.with.property("status", 400);
    });

    it("should respond with positive json for successful upload", async () => {
      const req = mockReq({
        params: { 0: "/" },
        files: [{
          originalname: "L2ZpbGUudHh0",
          path: "/",
        }],
      });
      const res = mockRes();

      await FilesUpload(req, res, err => { throw err; });
      res.json.should.be.calledWith({ status: 200, error: null });
    });
  });
});
