import { mockReq, mockRes } from "sinon-express-mock";
import { expect } from "chai";
import { get as FilesGet } from "App/api/http/controllers/files";
import { describeStructure } from "Tests/setup";

describeStructure(["HTTP", "Controllers"], () => {
  describe("FilesController.get", () => {
    it("should respond with 400 for /", () => {
      const req = mockReq({ url: "/" });
      const res = mockRes();
      const get = FilesGet.bind(null, req, res, err => { throw err; });

      expect(get).to.throw().with.property("status", 400);
    });

    it("should call sendFile for valid URL", () => {
      const req = mockReq({ url: "/aaaaaaaaaa" });
      const res = mockRes();

      FilesGet(req, res, err => { throw err; });

      res.sendFile.should.be.calledWith("aaaaaaaaaa");
    });
  });
});
