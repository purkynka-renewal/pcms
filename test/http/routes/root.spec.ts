import "reflect-metadata";
import request from "supertest";
import expressLoader from "App/loaders/express";
import { describeStructure } from "Tests/setup";

const app = expressLoader({ baseUrl: "/" });

describeStructure(["HTTP", "Routes"], () => {
  describe("GET /", () => {
    it("should say 'Hello :)'", done => {
      request(app)
        .get("/")
        .expect(200)
        .expect("Hello :)", done);
    });
  });
});
