import { mockReq, mockRes } from "sinon-express-mock";
import sinon from "sinon";
import authorization from "App/api/http/middlewares/authorization";
import { describeStructure } from "Tests/setup";

describeStructure(["HTTP", "Middlewares"], () => {
  describe("Authorization middleware", () => {
    it("should respond with 403 and stop for no body", () => {
      const req = mockReq({});
      const res = mockRes();
      const next = sinon.stub();

      authorization(req, res, next);
      res.status.should.be.calledWith(403);
      next.should.not.be.called;
    });

    it("should continue processing if sessionID is in req.body", () => {
      const req = mockReq({
        body: {
          sessionID: "a",
        },
      });
      const res = mockRes();
      const next = sinon.stub();

      authorization(req, res, next);
      res.status.should.not.be.called;
      next.should.be.called;
    });
  });
});
