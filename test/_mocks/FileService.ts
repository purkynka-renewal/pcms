import FileService from "App/services/files";
import sinon from "sinon";

const FileServiceMock: Omit<FileService, "emitter" | "prisma" | "config"> = {
  getId: sinon.stub().returns(Promise.resolve(0)),
  get: sinon.stub().returns(Promise.resolve({})),
  getImageSizes: sinon.stub().returns([1]),
  list: sinon.stub().returns(Promise.resolve({ files: [], folders: [] })),
  rename: sinon.stub().returns(Promise.resolve()),
  mkdir: sinon.stub().returns(Promise.resolve(0)),
  rm: sinon.stub().returns(Promise.resolve(true)),
  rmdir: sinon.stub().returns(Promise.resolve(true)),
  traverseFolders: sinon.stub().returns(Promise.resolve(0)),
  upload: sinon.stub().returns(Promise.resolve()),
  create: sinon.stub().returns(Promise.resolve({ id: "a", ext: "txt" })),
  optimizeImage: sinon.stub().returns(Promise.resolve("file")),
  generateThumbnail: sinon.stub().returns(Promise.resolve("file")),
  getImageCacheFilepath: sinon.stub().returns(Promise.resolve("/path/to/whatever")),
  getUploadFilepath: sinon.stub().returns(Promise.resolve("/path/to/whatever")),
};
export default FileServiceMock;
