import sinon from "sinon";
import AuthService from "App/services/auth";
import Session from "App/services/auth/Session";
import SessionMock from "./Session";

const AuthServiceMock: Omit<AuthService, "prisma"> = {
  auth: sinon.stub().returns(Promise.resolve(true)),
  createSession: sinon.stub().returns({ /* Session */ }),
  restoreSession: sinon.spy((str: string) => Object.assign(SessionMock, { authorized: !!str }) as Session),
};
export default AuthServiceMock;
