import EventEmitter from "events";
import Session from "App/services/auth/Session";
import sinon from "sinon";

const SessionMock = Object.assign<EventEmitter, Omit<Session, keyof EventEmitter>>(
  {} as EventEmitter,
  {
    connected: true,
    authorized: true,
    fresh: true,
    id: "",
    dead: false,
    destroy: sinon.stub(),
    scheduleDeath: sinon.stub(),
    cancelDeath: sinon.stub(),
    login: sinon.stub().returns(Promise.resolve(true)),
    logout: sinon.stub(),
  }
);
export default SessionMock;
