# Socket API of PCMS

The socket API is meant for fast interaction with admin panel and
all events except for the system ones are private and require your session to be authorized.
It can also be used to listen for content updates even without authorization.

Legend:
- Client events are triggered by the client and server events by server.
- All client events are expected to have a callback
- Callbacks recieve 2 arguments:
    - The return value or `null`
    - Error if any occured

### System
- `sys:session` (Server) - A session has been created
    - `id` - `String` - the ID of the session
- `sys:authorized` (Server) - The session has been successfully authorized
- `sys:logout` (Client) - Destroy current session (the connection will be dropped as well)
- `sys:isAuthorized` (Client) - Get boolean indicating if the current session is authorized
- `sys:login` (Client) - Atempt to authorize  
  **Returns:** `boolean` - Indicating if authorization was successfull
    - `username` - `String`  
    - `password` - `String`

### Content
- `content:updated` (Server) - Content has been updated
    - `modelName` - `string` - Name of the model that updated
    - `id`?       - `any`    - (should be ignored for singletons) ID of the row that was updated
- `content:get` (Client) - Get content item
    - `id`?    - `any`      - (ignored for singletons) ID of the row to return
    - `fields` - `string[]` - Which fields to return
- `content:set` (Client) - Change value of a field in an content item
    - `patch`   - `Record<string, any>` - The data to update
    - `id`?     - `any`      - (ignored for singletons) ID of the row to change
    - `fields?` - `string[]` - Which fields to return, only ID by default

### Files
- `files:updated` (Server) - Files have been updated
    - `path` - `string` - Path of the file that was updated/created
- `files:get` (Client) - Get data about a file
    - `path` - `string` - The path of the file
- `files:getImageSizes` (Client) - Get supported image sizes for image optimalization
- `files:list` (Client) - List files in a folder
    - `path` - `string` - The path of the folder    
- `files:rename` (Client) - Rename a file/folder
    - `id`   - `string|number`    
    - `name` - `string`
- `files:mkdir` (Client) - Create a folder
    - `path` - `string` - The path of the folder  
- `files:rmdir` (Client) - Remove a folder and it's contents  
    **WARNING**: Will burn your house down on a complex folder structure
    - `id` - `string` - The ID or a path of the folder
- `files:rmdir` (Client) - Remove a file or a folder and it's contents  
    **WARNING**: Will burn your house down on a complex folder structure (might use `files:rmdir` internally)
    - `id` - `string` - The ID or a path of the file or folder
