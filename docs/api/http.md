# HTTP (REST) API of PCMS

### Files
- **`POST`** `/files/*` - Upload a file or files to spceified path (e.g. `POST /files/logos/sps.png`)
    - The folder doesn't have to exist, it will be created
    - Only supported content type of body is `multipart/form-data`
    - The body must have a `files` field with the files you want to upload
    - Folder upload supported, add relative path infront of filename
    - Filename with optional relative path have to be base64 encoded
    - This is a private API, you have to send a `sessionID` query parameter
